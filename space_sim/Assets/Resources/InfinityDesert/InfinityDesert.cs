﻿using UnityEngine;
using System.Collections;
using MyHelps;

public class InfinityDesert : MonoBehaviour
{
    public Terrain terrainF, terrainM, terrainB;
    public float TerrainHeightOffset;
    public Transform Player;
    Vector3 _terrainWH;

    Terrain[] _terrains;
    BoxCollider[] _colliderBlocks;

#if UNITY_EDITOR
    MyHelp.ShowMeshBounds _showMeshBounds;
#endif

    private enum TERRAIN_BLOCK
    {
        front,
        middle,
        back,
        none,
    }

    // Use this for initialization
    void Start()
    {
        _terrainWH = terrainM.terrainData.size;

        //init terrain order
        _terrains = new Terrain[(int)TERRAIN_BLOCK.none];
        _terrains[(int)TERRAIN_BLOCK.front] = terrainF;
        _terrains[(int)TERRAIN_BLOCK.middle] = terrainM;
        _terrains[(int)TERRAIN_BLOCK.back] = terrainB;
        _resetColliderBlocks();

        _centeredTerrain(Player.position, Player.position);
        _resetTerrainNeighbor();
    }

    // Update is called once per frame
    void Update()
    {
        if (Player == null)
            return;

        if (Input.GetKeyUp(KeyCode.A))
        {

        }

        TERRAIN_BLOCK currentBlock = _considerInBlock(Player.position, _colliderBlocks);
        if (currentBlock != TERRAIN_BLOCK.middle)
        {
            //process new block setting...
            _resetTerrainOrder(currentBlock);

            Vector3 pos = _terrains[(int)TERRAIN_BLOCK.middle].transform.position;
            pos.x += _terrainWH.x * 0.5f;
            pos.z += _terrainWH.z * 0.5f;
            _centeredTerrain(pos, Player.position);
            _resetTerrainNeighbor();
        }



#if UNITY_EDITOR
        //always set y
        float playerY = Player.position.y;
        for (int a = 0; a < _terrains.Length; a++)
        {
            Vector3 pos = _terrains[a].transform.position;
            pos.y = playerY + TerrainHeightOffset;
            _terrains[a].transform.position = pos;
        }

        //draw debug bound box
        if (_showMeshBounds == null)
            _showMeshBounds = new MyHelp.ShowMeshBounds();
        _showMeshBounds.color = Color.white;
        foreach (BoxCollider col in _colliderBlocks)
            _showMeshBounds.DrawBox(col.bounds, Matrix4x4.identity);
#endif

    }

    void _resetTerrainOrder(TERRAIN_BLOCK newBlock)
    {
        Debug.Log("[InfinityDesert] _resetTerrainOrder()...newBlock: " + newBlock);

        if (newBlock == TERRAIN_BLOCK.front)
        {
            //new[M] = old[F]
            //new[F] = old[B]
            //new[B] = old[M]
            Terrain oldM = _terrains[(int)TERRAIN_BLOCK.middle];
            Terrain oldF = _terrains[(int)TERRAIN_BLOCK.front];
            Terrain oldB = _terrains[(int)TERRAIN_BLOCK.back];
            _terrains[(int)TERRAIN_BLOCK.middle] = oldF;
            _terrains[(int)TERRAIN_BLOCK.front] = oldB;
            _terrains[(int)TERRAIN_BLOCK.back] = oldM;
        }
        else if (newBlock == TERRAIN_BLOCK.back)
        {
            //new[M] = old[B]
            //new[F] = old[M]
            //new[B] = old[F]
            Terrain oldM = _terrains[(int)TERRAIN_BLOCK.middle];
            Terrain oldF = _terrains[(int)TERRAIN_BLOCK.front];
            Terrain oldB = _terrains[(int)TERRAIN_BLOCK.back];
            _terrains[(int)TERRAIN_BLOCK.middle] = oldB;
            _terrains[(int)TERRAIN_BLOCK.front] = oldM;
            _terrains[(int)TERRAIN_BLOCK.back] = oldF;
        }
        else
            Debug.LogError("[InfinityDesert] _resetMiddleTerrain() : newBlock must be front or back, because player just can move forward|backward, maybe player moving too fast...");


        _resetColliderBlocks();
        _resetTerrainNeighbor();
    }

    void _resetColliderBlocks()
    {
        if (_colliderBlocks == null)
            _colliderBlocks = new BoxCollider[_terrains.Length];
        for (int a = 0; a < _terrains.Length; a++)
            _colliderBlocks[a] = _terrains[a].GetComponent<BoxCollider>();
    }


    /// <summary>
    /// Set middle terrain is centered as Player's position.
    /// </summary>
    void _centeredTerrain(Vector3 centerPos, Vector3 playerPos)
    {
        Vector3 pos = centerPos;//Player.position;
        pos.y = playerPos.y + TerrainHeightOffset;
        pos.x -= _terrainWH.x * 0.5f;
        pos.z -= _terrainWH.z * 0.5f;

        _terrains[(int)TERRAIN_BLOCK.middle].transform.position = pos;

        pos.z += _terrainWH.z;
        _terrains[(int)TERRAIN_BLOCK.front].transform.position = pos;

        pos.z -= _terrainWH.z * 2;
        _terrains[(int)TERRAIN_BLOCK.back].transform.position = pos;

        Debug.Log("_centeredTerrain");

        //TERRAIN_BLOCK currentBlock = _considerInBlock(playerPos, _colliderBlocks);
        //if (currentBlock != TERRAIN_BLOCK.middle)
        //    Debug.LogError("[InfinityDesert] _centeredTerrain() : player _currentBlock must in middle block...");
    }

    /// <summary>
    /// Set Unity terrain's neighbor data, for it's LOD connected.
    /// </summary>
    void _resetTerrainNeighbor()
    {
        _terrains[(int)TERRAIN_BLOCK.front].SetNeighbors(null, null, null, _terrains[(int)TERRAIN_BLOCK.middle]);
        _terrains[(int)TERRAIN_BLOCK.middle].SetNeighbors(null, _terrains[(int)TERRAIN_BLOCK.front], null, _terrains[(int)TERRAIN_BLOCK.back]);
        _terrains[(int)TERRAIN_BLOCK.back].SetNeighbors(null, _terrains[(int)TERRAIN_BLOCK.middle], null, null);
    }

    static TERRAIN_BLOCK _considerInBlock(Vector3 pos, BoxCollider[] blocks)
    {
        int count = 0;
        int blockID = (int)TERRAIN_BLOCK.none;

        foreach (BoxCollider col in blocks)
        {
            if (col.bounds.Contains(pos))
            {
                blockID = count;
                break;
            }
            count++;
        }

       // Debug.Log("blockID: " + ((TERRAIN_BLOCK)blockID).ToString());
        return (TERRAIN_BLOCK)blockID;
    }
}
