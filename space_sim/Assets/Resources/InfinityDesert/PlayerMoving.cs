﻿using UnityEngine;
using System.Collections;

public class PlayerMoving : MonoBehaviour
{
    public iTweenPath TweenPath;
    iTweenEvent _tweenEvent;

    public InfinityDesert InfinityTerrain;

    // Use this for initialization
    void Start()
    {
        _tweenEvent = GetComponent<iTweenEvent>();
        _tweenEvent.Play();
        
        transform.position = TweenPath.nodes[0];

        InfinityTerrain.gameObject.GetComponent<InfinityDesert>().enabled = true;
    }

    // Update is called once per frame
    void Update()
    {

    }

    [ContextMenu("Restart")]
    void _restart()
    {
       // iTweenEvent.GetEvent(this.gameObject, _tweenEvent.name).Start();
    }
}
