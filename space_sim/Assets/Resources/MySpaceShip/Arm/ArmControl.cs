﻿using UnityEngine;
using System.Collections;

public class ArmControl : MonoBehaviour
{
    public MyFlyController FlyController;
    public Animator Ani;
    public bool ShootOnce;
    bool _shooting;
    public Transform RayPoint;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        float horizontal, vertical;
        FlyController.GetAxisRatio(out horizontal, out vertical);
        Ani.SetFloat("Horizontal", horizontal);
        Ani.SetFloat("Vertical", -vertical);



        Ani.SetBool("Shoot", false);
        if (Input.GetKey(KeyCode.Space) || FlyController.Shooting())
        {
            if (!_shooting)
                Ani.SetBool("Shoot", true);
        }
        _shooting = FlyController.Shooting();

        MyHelpDraw.LineDraw(RayPoint, RayPoint.position + RayPoint.forward * 99999f, 0.1f, 0.1f, Color.red);
    }
}
