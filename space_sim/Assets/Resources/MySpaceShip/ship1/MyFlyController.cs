﻿using UnityEngine;
using System.Collections;

public class MyFlyController : MonoBehaviour
{
    //public enum SetWay
    //{
    //    forward,
    //    up,
    //}
    public Transform ControlBar;
    public Transform Bar_Horizontal;
    public Transform Bar_Vertical;
    //public SetWay UpAxis;
    //public SetWay ForwardAxis;

    //public float exp = 2;

    public float MaxDegree = 45;
    public SteamVR_TrackedObject ViveController;

    public AnimationCurve curve;
    public bool IgnoreVertical = false;
    //public bool Clamp90 = true;

    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    bool _shooting;
    void Update()
    {
        if (!_shooting)
        {
            if (ViveHelp.IsTriggerPress(ViveController, ViveHelp.PressState.Down))
                _shooting = true;
        }
        if (_shooting)
        {
            if (ViveHelp.IsTriggerPress(ViveController, ViveHelp.PressState.Up))
                _shooting = false;
        }
    }


    public bool Shooting()
    {
        return _shooting;
    }

    [Tooltip("Make the controller type just like the Matrix 3 movie : https://www.youtube.com/watch?v=jk3Z-MVoUg4" +
        "\n,if false, will be the fly joystick type")]
    public bool TheMatrixRobotMode = false;

    public void GetAxisRatio(out float outHRatio, out float outVRatio)
    {
        outVRatio = outHRatio = 0;

        if (!this.enabled)
        {
            //Debug.LogWarning("[MyFlyController] : !this.enabled");
            return;
        }

        if (
            ViveController == null ||
            (ViveController != null && !ViveController.gameObject.activeSelf) ||
            (ViveController != null && !ViveHelp.IsTracking(ViveController))
            )
        {
            //Debug.Log("enable : " + ViveController.enabled + " , isActiveAndEnabled : " + ViveController.isActiveAndEnabled + " , index : " + ViveController.index);
            //Debug.Log("activeSelf : " + ViveController.gameObject.activeSelf + " , IsTracking : " + ViveHelp.IsTracking(ViveController));
            outVRatio = Input.GetAxis("Vertical");
            outHRatio = Input.GetAxis("Horizontal");

            //Debug.LogWarning("[MyFlyController] : use Input.GetAxis");
            return;
        }

        if (ViveController != null)
        {
            Bar_Vertical.localRotation = Bar_Horizontal.localRotation = Quaternion.identity;

            //Bar_Horizontal
            Vector3 viveLocalRight = ViveController.transform.parent.InverseTransformDirection(ViveController.transform.right);
            Vector3 viveLocalBackward = ViveController.transform.parent.InverseTransformDirection(ViveController.transform.up);//vive's up is his backward
            Vector3 viveLocalUp = ViveController.transform.parent.InverseTransformDirection(ViveController.transform.forward);//vive's forward is his up
            if (TheMatrixRobotMode)
            {
                float angleRight = Vector3.Angle(Vector3.right, viveLocalRight) - 90;
                Bar_Horizontal.localRotation = Quaternion.Euler(0, angleRight, 0);
            }
            else
            {   
                //float dot = Vector3.Dot(viveLocalUp, Vector3.up);
                //if (dot > 0 || !Clamp90)
                {
                    float angleRight = Vector3.Angle(Vector3.up, viveLocalRight) - 90;
                    Bar_Horizontal.localRotation = Quaternion.Euler(0, 0, -angleRight);
                }
                //else
                //{   
                //    dot = Vector3.Dot(Vector3.up, viveLocalRight);
                //    //Debug.Log("fffff : " + dot);
                //    Bar_Horizontal.localRotation = Quaternion.Euler(
                //        0
                //        , 0
                //        , 90 * ((dot > 0) ? 1.0f : -1.0f));//clamp angle
                //}
            }

            if (!IgnoreVertical)
            {
                //Bar_Vertical                
                //float dot = Vector3.Dot(viveLocalUp, Vector3.up);
                //if (dot > 0 || !Clamp90)
                {
                    float angleForward = Vector3.Angle(Vector3.up, viveLocalBackward) - 90;
                    Bar_Vertical.localRotation = Quaternion.Euler(-angleForward, 0, 0);
                }
                //else
                //{
                //    dot = Vector3.Dot(Vector3.up, viveLocalBackward);
                //    Bar_Vertical.localRotation = Quaternion.Euler(Mathf.Acos(dot) * Mathf.Rad2Deg, 0, 0);//clamp angle
                //    //Bar_Vertical.localRotation = Quaternion.Euler(90 * ((dot > 0) ? 1.0f : -1.0f)
                //    //    , 0, 0);//clamp angle

                //}
            }
        }
        Vector3 localUp = this.transform.InverseTransformDirection(ControlBar.up);
        Vector3 localForward = this.transform.InverseTransformDirection(ControlBar.forward);
        Vector3 localRight = this.transform.InverseTransformDirection(ControlBar.right);

        Vector3 localVertical = localUp;
        localVertical.x = 0;
        localVertical.Normalize();
        float verticalAngle = Vector3.Angle(localVertical, Vector3.up);
        //Debug.Log("verticalAngle : " + verticalAngle);
        outVRatio = Mathf.Clamp01(verticalAngle / MaxDegree);
        //outVRatio = Mathf.Pow(outVRatio, exp);
        float t = outVRatio;                 // the position on the x-axis
        outVRatio = curve.Evaluate(t);  // gets the position on the y-axis
        float verticalWay = Vector3.Dot(localForward, Vector3.up);
        if (verticalWay > 0)
            outVRatio = -outVRatio;
        //Debug.Log("outVRatio : " + outVRatio + " , verticalWay : " + verticalWay);

        if (TheMatrixRobotMode)
        {
            Vector3 localHorizontal = localForward;
            localHorizontal.y = 0;
            localHorizontal.Normalize();
            float horizontalAngle = Vector3.Angle(localHorizontal, Vector3.forward);
            outHRatio = Mathf.Clamp01(horizontalAngle / MaxDegree);
            //outHRatio = Mathf.Pow(outHRatio, exp);
            t = outHRatio;                 // the position on the x-axis
            outHRatio = curve.Evaluate(t);  // gets the position on the y-axis
            float horizontalWay = Vector3.Dot(localRight, Vector3.forward);
            if (horizontalWay > 0)
                outHRatio = -outHRatio;
            //Debug.Log("outHRatio : " + outHRatio + " , horizontalWay : " + horizontalWay);
        }
        else
        {
            Vector3 localHorizontal = localUp;
            localHorizontal.z = 0;
            localHorizontal.Normalize();
            float horizontalAngle = Vector3.Angle(localHorizontal, Vector3.up);
            outHRatio = Mathf.Clamp01(horizontalAngle / MaxDegree);
            //outHRatio = Mathf.Pow(outHRatio, exp);
            t = outHRatio;                 // the position on the x-axis
            outHRatio = curve.Evaluate(t);  // gets the position on the y-axis
            float horizontalWay = Vector3.Dot(localRight, Vector3.up);
            if (horizontalWay > 0)
                outHRatio = -outHRatio;
            //Debug.Log("outHRatio : " + outHRatio + " , horizontalWay : " + horizontalWay);
        }
        //Debug.Log("horizontalWay : " + horizontalWay + " , verticalWay : " + verticalWay);
        //Debug.Log("horizontalAngle : " + horizontalAngle + " , verticalAngle : " + verticalAngle);
        //Debug.Log("Hratio : " + outHRatio + " , Vratio : " + outVRatio);
    }

}
