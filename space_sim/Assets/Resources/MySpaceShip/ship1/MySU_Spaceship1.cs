﻿using UnityEngine;
using System.Collections;

public class MySU_Spaceship1 : SU_Spaceship
{
    public MyFlyController FlyController;
    public bool AlwaysStart = true;

    protected void FixedUpdate()
    {
        if (FlyController == null)
            return;

        base.FixedUpdate();

        float horizontal, vertical;
        FlyController.GetAxisRatio(out horizontal, out vertical);

        // In the physics update...
        // Add relative rotational roll torque when steering left/right
        _cacheRigidbody.AddRelativeTorque(new Vector3(0, 0, -horizontal * rollRate * _cacheRigidbody.mass));
        // Add rudder yaw torque when steering left/right
        _cacheRigidbody.AddRelativeTorque(new Vector3(0, horizontal * yawRate * _cacheRigidbody.mass, 0));
        // Add pitch torque when steering up/down
        _cacheRigidbody.AddRelativeTorque(new Vector3(vertical * pitchRate * _cacheRigidbody.mass, 0, 0));
    }

    protected void Update()
    {
        base.Update();

        if (AlwaysStart)
        {
            foreach (SU_Thruster _thruster in thrusters)
            {
                _thruster.StartThruster();
            }
        }

        if (FlyController.Shooting())
        {
            //Debug.Log("firefirefirefirefirefirefirefirefirefirefirefirefirefirefirefirefirefire");

            // Itereate through each weapon mount point Vector3 in array
            foreach (Vector3 _wmp in weaponMountPoints)
            {
                // Calculate where the position is in world space for the mount point
                Vector3 _pos = transform.position + transform.right * _wmp.x + transform.up * _wmp.y + transform.forward * _wmp.z;
                // Instantiate the laser prefab at position with the spaceships rotation
                Transform _laserShot = (Transform)Instantiate(laserShotPrefab, _pos, transform.rotation);
                // Specify which transform it was that fired this round so we can ignore it for collision/hit
                _laserShot.GetComponent<SU_LaserShot>().firedBy = transform;

            }
            // Play sound effect when firing
            if (soundEffectFire != null)
            {
                GetComponent<AudioSource>().PlayOneShot(soundEffectFire);
            }
        }
    }
}
