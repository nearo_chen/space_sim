﻿using UnityEngine;
using System.Collections;

public class ShipController1 : MonoBehaviour
{
    public Rigidbody ShipRB;
    public float HSpeed;
    public float VSpeed;
   // public float VSpeedRot;
    public Camera ChasingCamera;
    public MyFlyController FlyController;
    

    // Use this for initialization
    void Start()
    {
        ShipRB = this.GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float horizontal, vertical;
        FlyController.GetAxisRatio(out horizontal, out vertical);
        //Debug.Log("horizontal : " + horizontal + " , vertical : " + vertical);
        
        Vector3 movement = new Vector3(horizontal * HSpeed, -vertical * VSpeed, 0.0f);
        movement = ChasingCamera.transform.TransformVector(movement);        
        ShipRB.AddForce(movement);

       // FlyController.transform.position = transform.position;
        //FlyControllerMain.transform.rotation = ChasingCamera.transform.rotation;
    }
}
